from typing import Sequence


def get_words(text: str) -> Sequence[str]:
    return text.split()


def get_text(reversed_words: Sequence[str]) -> str:
    return " ".join(reversed_words)


def get_revers_word(word: str) -> str:
    """
    Get reverced word (alpha symbols - revers, non-alpha symbols - stays on old places.)

    Usage examoles:
    >>> get_revers_word('abcd')
    'dcba'
    >>> get_revers_word('a1bcd')
    'd1cba'
    >>> get_revers_word(12)
    Traceback (most recent call last):
    ...
    TypeError: invalid data type for get_revers_word(). Expected 'str', but got <class 'int'>
    """
    if not isinstance(word, str):
        raise TypeError(f"invalid data type for get_revers_word(). Expected 'str', but got {type(word)}")

    result = ""
    letters = [letter for letter in word if letter.isalpha()]
    for symbol in word:
        result += letters.pop() if symbol.isalpha() else symbol
    return result


def tricky_revers(text: str) -> str:
    if not isinstance(text, str):
        raise TypeError(f"invalid data type for tricky_revers(). Expected 'str', but got {type(text)}")

    words = get_words(text)
    return get_text(map(get_revers_word, words))


if __name__ == "__main__":
    cases = (
        ("", ""),
        ("abcd", "dcba"),
        ("a1bcd", "d1cba"),
        ("abcd efgh", "dcba hgfe"),
        ("a1bcd efg!h", "d1cba hgf!e"),
    )

    for argument, result in cases:
        assert tricky_revers(argument) == result, f"ERROR! -> \
            tricky_revers({argument}) = {tricky_revers(argument)}, but expected: {result}"
