import pytest
from tricky_revers import tricky_revers


@pytest.mark.parametrize("argument, result", (
    ("", ""),
    ("abcd", "dcba"),
    ("a1bcd", "d1cba"),
    ("abcd efgh", "dcba hgfe"),
    ("a1bcd efg!h", "d1cba hgf!e"),
))
def test_typical_cases(argument, result):
    assert tricky_revers(argument) == result, f"ERROR! -> \
                tricky_revers({argument}) = {tricky_revers(argument)}, but expected: {result}"


cases = (
        None,
        True,
        [],
        12,
        100.001
    )


@pytest.mark.parametrize("argument", cases)
def test_atypical_cases(argument):
    with pytest.raises(TypeError) as error:
        tricky_revers(argument)
        print(error)



if __name__ == "__main__":
    pytest.main()
